#!/usr/bin/python

import json
import os
import sys


def log(**kwargs):
    print(json.dumps(kwargs))

def debug(**kwargs):
    kwargs.update({'message_type': 'debug'})
    log(**kwargs)

def error(**kwargs):
    kwargs.update({'message_type': 'error'})
    log(**kwargs)

try:
    nomad_addr = os.environ['NOMAD_ADDR']
except KeyError:
    error(comment='Error obtaining Nomad address!')
    sys.exit(1)

log(comment='Hello world!')
