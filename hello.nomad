job "${PREFIX}_hello" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_hello" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/hello_world/hello:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_hello"
        }
      }

    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      TAG = "${PREFIX}_hello"
    }

    resources {
      cpu = 100 # Mhz
      memory = 128 # MB
    }

  }
}
